package rasus.projekt;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import rasus.projekt.sharedresource.SharedResource;

import java.io.IOException;

public class CustomConsumer extends DefaultConsumer {
    private SharedResource sharedResource;
    private Channel channel;
    private String userName;
    private String token;
    private String queueName;

    public CustomConsumer(Channel channel, SharedResource sharedResource, String userName, String token, String queueName) {
        super(channel);
        this.sharedResource = sharedResource;
        this.channel = channel;
        this.userName = userName;
        this.token = token;
        this.queueName = queueName;
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        String message = new String(body, "UTF-8");
        System.out.println(" [x] Received message '" + message + "'");
        if (message.equals(token)) {
            System.out.println(" [x] Token received. Do work...");
            doWork();
            channel.basicAck(envelope.getDeliveryTag(), false);
            channel.basicPublish("", queueName, null, token.getBytes());
            System.out.println(" [x] Token sent");
            System.out.println("Waiting for token...");
        }
    }

    public void doWork() throws IOException {
        this.sharedResource.synchronize();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String item = this.sharedResource.getItem(userName);
        System.out.println(" [x] Taken item: " + item);


    }
}
