package rasus.projekt.sharedresource;

public interface SharedResource {
    public String getItem(String userName);
    public void synchronize();
}
