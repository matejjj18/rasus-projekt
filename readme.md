<h1>
26. Međusobno isključivanje primjenom prstena
</h1>

<h2>Zadatak</h2>
<strong>Voditelj:</strong> Ivana Podnar Žarko<br>
<strong>Br. studenata:</strong> 2 studenta (Matej Jelušić, Mislav Gillinger)<br>
<strong>Opis teme:</strong> Vaš je zadatak implementirati algoritam za međusobno isključivanje primjenom
prstena koji je definiran na 5. predavanju za pristup nekon dijeljenom sredtsvu. Za implementaciju
komunikacije između procesa na prstenu koristite komunikaciju porukama (rep za poruke).
Preporučuje se koristiti protokol AMQP (Advanced Message Queuing Protocol) i RabbitMQ. Sami
odaberite što će biti dijeljeno sredstvo i njegovu namjenu.<br><br>
Početna literatura:<br>
https://www.rabbitmq.com/resources/specs/amqp0-9.pdf<br>
https://www.rabbitmq.com/

<br>
<hr>
<h2>Teorijska pretpostavka rješenja</h2>

<p>
Svi procesi će biti povezani u logičku cjelinu koja je zasnovana na prstenu. Procesi razmjenjuju
   značku duž prstena, a samo onaj proces koji ima značku može pristupiti dijeljenom sredstvu. Kada je
   proces završio s poslom, prosljeđuje značku susjednom procesu u prstenu.
</p>
   
   <p>Proces će biti modeliran razredom koji sadrži metode poput čitaj iz spremnika, piši u spremnik,
      proslijedi značku, a svaki proces će također imati identifikator. Dijeljeno sredstvo će također biti
      modelirano razredom, a pohrana će se odvijati u neku internu strukturu podataka. 
   </p>
   
![Model](assets/model.JPG)

<p>Rješenje će biti ostvareno u programskom jeziku Java, uz pomoć RabbitMQ, te koristeći IntelliJ
   razvojno okruženje.
   U nastavku je poveznica na upute za korištenje RabbitMQ u kojoj se mogu pronaći uputstva I primjeri
   za programski jezik Java</p>
   
   
   
   <div>
   <strong>Mislav Gillinger </strong><br>
   <strong>Matej Jelušić</strong>
   
   </div>